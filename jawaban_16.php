CREATE TABLE customers (
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255)
)

CREATE TABLE orders (
    id int AUTO_INCREMENT PRIMARY KEY,
    amount varchar(255),
    customer_id int, 
    FOREIGN KEY REFERENCES customers(id)
)


PersonID int FOREIGN KEY REFERENCES Persons(PersonID)